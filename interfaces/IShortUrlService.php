<?php


namespace app\interfaces;


use app\models\forms\CreateShortUrlForm;

interface IShortUrlService
{
    public function create(CreateShortUrlForm $data);
    public function get($hash);
}
