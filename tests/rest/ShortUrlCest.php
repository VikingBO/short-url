<?php

namespace rest;


use app\tests\fixtures\ShortUrlFixtures;
use RestTester;
use Yii;

/**
 * Class ShortUrlCest
 * @package rest
 *
 * @noinspection PhpUnused
 */
class ShortUrlCest
{
    public function _fixtures()
    {
        return [
            'shortUrl' => ShortUrlFixtures::class
        ];
    }

    public function testShortUrl(RestTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/link/create', [
            'url' => Yii::$app->security->generateRandomString(2083)
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('$.hash');
        $shortUrl = json_decode($I->grabResponse());

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('/link/' . $shortUrl->hash);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('$.url');
    }
}
