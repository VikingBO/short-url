<?php


namespace app\tests\fixtures;


use app\models\ShortUrl;
use yii\test\ActiveFixture;

class ShortUrlFixtures extends ActiveFixture
{
    const SHORT_URL = ['hash' => 'hash12', 'url' => 'http://test.local'];

    public $modelClass = ShortUrl::class;
}
