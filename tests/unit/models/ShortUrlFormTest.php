<?php


namespace unit\models;


use app\models\forms\CreateShortUrlForm;
use app\models\ShortUrl;
use app\services\ShortUrlService;
use app\tests\fixtures\ShortUrlFixtures;
use Codeception\Specify;
use Codeception\Test\Unit;

class ShortUrlFormTest extends Unit
{
    use Specify;

    /**
     * @var CreateShortUrlForm
     */
    private $form;
    /**
     * @var ShortUrlService
     */
    private $service;

    public function _before()
    {
        $this->form = new CreateShortUrlForm();
        $this->service = new ShortUrlService();
    }

    public function _fixtures()
    {
        return [
            'shortUrl' => ShortUrlFixtures::class
        ];
    }

    public function testValidation()
    {
        $this->specify('fields are required', function () {
            expect('model is not valid', $this->form->validate())->false();
            expect('url has error', $this->form->getErrors())->hasKey('url');
        });

        $this->specify('fields are wrong', function () {
            $this->form->url = \Yii::$app->security->generateRandomString(2084);
            expect('model is not valid', $this->form->validate())->false();
            expect('url has error', $this->form->getErrors())->hasKey('url');
        });
    }

    public function testCreateShortUrl()
    {
        $this->form->load(['url' => \Yii::$app->security->generateRandomString(2083)], '');
        $shortUrl = $this->service->create($this->form);
        $this->assertNotEmpty($shortUrl, 'ShortUrl created');
        $getShortUrl = $this->service->get($shortUrl->hash);
        $this->assertNotEmpty($getShortUrl, 'Short url found');
    }
}
