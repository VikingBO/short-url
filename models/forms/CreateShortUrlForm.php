<?php


namespace app\models\forms;


class CreateShortUrlForm extends \yii\base\Model
{
    public $url;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'string', 'max' => 2083],
        ];
    }
}
