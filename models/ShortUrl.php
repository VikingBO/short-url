<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%short_url}}".
 *
 * @property int $id
 * @property string $hash
 * @property string $url
 * @property int|null $expired_at
 * @property int|null $follow_count
 * @property int $created_at
 * @property int|null $updated_at
 */
class ShortUrl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%short_url}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hash' => 'Hash',
            'url' => 'Url',
            'expired_at' => 'Expired At',
            'follow_count' => 'Follow Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At'
        ];
    }
}
