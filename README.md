##Short links RESTfull API

API генерации и проверки коротких ссылок сайта

```http request
GET /api/v1/link/{hash}
hash - уникальный идентификатор ссылки (required)
```

```http request
POST /api/v1/link/create
url - ссылка для сокращения (required)
```
