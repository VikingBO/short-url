<?php


namespace app\controllers\api\v1;


use app\models\forms\CreateShortUrlForm;
use app\models\ShortUrl;
use app\services\ShortUrlService;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class LinkController extends \yii\rest\Controller
{
    public $shortUrlService;

    public function __construct($id, $module, ShortUrlService $shortUrlService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->shortUrlService = $shortUrlService;
    }

    protected function verbs(){
        return [
            'index' => ['GET'],
            'create' => ['POST']
        ];
    }

    public function actionIndex($hash)
    {
        if ($shortUrl = ShortUrl::findOne(['hash' => $hash])) {
            return $shortUrl;
        } else {
            throw new NotFoundHttpException();
        }
    }

    public function actionCreate()
    {
        $form = new CreateShortUrlForm();
        $form->load(\Yii::$app->request->getBodyParams(), '');

        if ($form->validate()) {
            try {
                $shortUrl = $this->shortUrlService->create($form);
                \Yii::$app->response->setStatusCode(200);
                return $shortUrl;
            } catch (\Exception $e) {
                throw new BadRequestHttpException($e->getMessage(), null, $e);
            }
        }

        return $form;
    }
}
