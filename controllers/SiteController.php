<?php

namespace app\controllers;

use app\services\ShortUrlService;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    public $linkService;

    public function __construct($id, $module, ShortUrlService $linkService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->linkService = $linkService;
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionShortUrl($hash)
    {
        $shortLink = $this->linkService->get($hash);

        if (!empty($shortLink)) {
            $this->linkService->updateFollow();

            header('Location: ' . $shortLink->url, false, '301');
            exit;
        }

        throw new NotFoundHttpException();
    }
}
