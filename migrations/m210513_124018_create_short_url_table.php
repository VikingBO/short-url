<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%short_url}}`.
 */
class m210513_124018_create_short_url_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%short_url}}', [
            'id' => $this->primaryKey(255),

            'hash' => $this->string(6)->notNull(),
            'url' => $this->string(2083)->notNull(),
            'expired_at' => $this->integer()->defaultValue(null),
            'follow_count' => $this->integer()->defaultValue(null),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->defaultValue(null)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%short_url}}');
    }
}
