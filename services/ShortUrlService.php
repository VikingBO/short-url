<?php


namespace app\services;


use app\models\forms\CreateShortUrlForm;
use app\models\ShortUrl;

class ShortUrlService implements \app\interfaces\IShortUrlService
{

    /**
     * @var ShortUrl
     */
    private $instance;

    /**
     * Create short url
     *
     * @param CreateShortUrlForm $data
     * @return ShortUrl
     * @throws \yii\base\Exception
     */
    public function create(CreateShortUrlForm $data)
    {
        $this->instance = new ShortUrl();
        $this->instance->url = $data->url;
        $this->instance->hash = \Yii::$app->security->generateRandomString(6);
        $this->instance->save();
        return $this->instance;
    }

    /**
     * Get short url info by hash
     *
     * @param $hash
     * @return ShortUrl|null
     */
    public function get($hash)
    {
        $this->instance = ShortUrl::findOne(['hash' => $hash]);
        return $this->instance;
    }

    public function updateFollow()
    {
        ++$this->instance->follow_count;
        return $this->instance->save();
    }
}
