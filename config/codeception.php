<?php

return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/web.php',
    require __DIR__ . '/test.php',

    [
        'components' => [
            'request' => [
                'cookieValidationKey' => 'mm9nfPFZFUu8y8rGGCoN2hoLldEClegn',
            ],
        ],
    ]
);
