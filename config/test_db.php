<?php
$db = require __DIR__ . '/db.php';
// test database! Important not to run tests on production or development databases
$db['dsn'] = 'sqlite:'.__DIR__.'/../data/test.db';
$db['username'] = '';
$db['password'] = '';

return $db;
